Vue.component('filters-search', {

    data: () => {
        return {
            minSearchTextLength: 2,
            dropdownShow: false,
            searchText: ''
        }
    },

    computed: {
        
        watchFiltersSelected () {
            this.searchText = this.$store.state.filtersSelected.search;
        },

        searchVariants () {
            let allVariants = this.$store.getters.filters.search;
            let searchVariants = [];
            let searchText = this.searchText || '';

            if (searchText.length >= this.minSearchTextLength) {
                allVariants.map((variant) => {
                    if (variant.toLowerCase().indexOf(searchText.toLowerCase()) === 0) {
                        searchVariants.push(variant);
                    }
                });
            }

            return searchVariants;
        },

        isShowVariants() {
            let searchText = this.searchText || '';

            if (searchText.length >= this.minSearchTextLength && this.searchVariants.length > 0) {
                return true;
            }

            return false;
        },

        isSelected() {
            let searchTextTyped = this.searchText || '';
            let searchTextStored = this.$store.state.filtersSelected.search;

            if (searchTextTyped === searchTextStored) {
                return 'active'
            }

            return '';
        }

    },

    methods: {

        setFiltersSelected(values) {
            this.$store.commit('updateFiltersSelected', values);
        },

        setFilterSearch(searchText, event) {
            if (searchText === '') {
                searchText = null;
            }

            this.$store.commit('updateFiltersSelected', {search: searchText});
            this.dropdownShow = false;
            event.target.blur();
        },

        returnCurrentText() {
            this.dropdownShow = false;
            this.searchText = this.$store.state.filtersSelected.search;
        },

        searchIsTyping() {
            this.dropdownShow = true;
            this.searchText = '';
        }
        
    },

    template: `
    <div class="dropdown" :data-watch="watchFiltersSelected">
        <div class="input-group input-group-search" data-flip=false>
            <input
                v-model="searchText"
                v-on:focus="searchIsTyping"
                v-on:keydown.enter="setFilterSearch(searchText, $event)"
                v-on:blur="returnCurrentText" type="text"
                :class="isSelected + ' ' + (isShowVariants ? 'border-radius-0' : '')"
                class="form-control"
                placeholder="Search for a matches"
                autocomplete="off">
            <div class="input-group-append">
                <button class="btn btn-search" type="button" v-on:mousedown="setFilterSearch(searchText, $event)">
                    <i class="icon icon-loupe"></i>
                </button>
            </div>
        </div>
        <div class="dropdown-menu with-search" :class="dropdownShow ? 'show' : ''" v-if="isShowVariants">
            <div class="dropdown-item" v-for="variant in searchVariants">
                <button class="btn btn-search-variant" v-on:mousedown="setFilterSearch(variant, $event)">
                    {{ variant }}
                </button>
            </div>
        </div>
    </div>
    `
})