Vue.component('filters-desktop', {

    computed: {

        timezoneOffsetMinutes () {
            return this.$store.state.timezoneOffsetMinutes;
        },

        filters () {
            return this.$store.getters.filters;
        },

        filtersSelected () {
            return this.$store.state.filtersSelected;
        },

        disabledDates() {
            let minDate = moment(moment.min(this.filters.dates)).subtract(1, 'days');
            let maxDate = moment(moment.max(this.filters.dates)).add(1, 'days');
            let filterDates = this.filters.dates;
            let timezoneOffsetMinutes = this.timezoneOffsetMinutes;

            return {
                to: minDate.toDate(),
                from: maxDate.toDate(),
                customPredictor: function (date) {
                    isReturn = true;

                    date = moment(date).utcOffset(timezoneOffsetMinutes).format('L');

                    filterDates.map((filterDate) => {
                        if (filterDate.format('L') === date) {
                            isReturn = false;
                        }
                    });

                    return isReturn;
                }
            }
        },

        minDate() {
            return this.filters.dates.length > 0 ? moment.min(this.filters.dates).toDate() : false;
        },

        datepickerLanguage() {
            return datepickerLanguage();
        }

    },

    methods: {

        setFiltersSelected(values) {
            this.$store.commit('updateFiltersSelected', values);
        },

        setDateDaySelected(date) {
            let values = {
                dateDay: moment(date).utc(),
                dateMonth: null
            }
            this.$store.commit('updateFiltersSelected', values);
        },

        clearFiltersSelected() {
            this.$store.commit('clearFiltersSelected');
        },

        groupClass(group) {
            return this.$store.getters.groupClass(group);
        }
        
    },

    filters: {
        
        // Format for date
        dateFormat(value) {
            return value.format('D MMMM');
        }
        
    },

    components: {
        vuejsDatepicker
    },

    template: `
        <div class="input-group input-group-filters">
            <div class="dropdown dropdown-with-arrow">
                <label class="btn-select-label" data-toggle="dropdown" data-flip=false>
                    <input
                        :class="'btn btn-select ' + (filtersSelected.team ? 'active' : '')"
                        type="button"
                        :value="filtersSelected.team ? filtersSelected.team : 'TEAM'"
                    >
                    <i class="icon icon-arrow"></i>
                </label>
                <div class="dropdown-menu with-scroll">
                    <div class="js-show-more-wrapper js-custom-scrollbar show-more-wrapper sm" data-show-more-target="team">
                        <div>
                            <button
                                v-for="team in filters.teams"
                                v-on:click="setFiltersSelected({team: team})"
                                :class="'btn btn-filter btn-block '+ (team === filtersSelected.team ? 'active' : '')">
                                    {{ team }}
                            </button>
                        </div>
                    </div>
                    <button class="js-show-more show-more" data-show-more-target="team" data-show-more-text="Show less" data-scroll-window="true">
                        Show more
                    </button>
                </div>
            </div>
            <div class="dropdown dropdown-with-arrow">
                <label class="btn-select-label" data-toggle="dropdown" data-flip=false>
                    <input
                        :class="'btn btn-select ' + (filtersSelected.group ? 'active' : '')"
                        type="button"
                        :value="filtersSelected.group ? filtersSelected.group : 'GROUP'"
                    >
                    <i class="icon icon-arrow"></i>
                </label>
                <div class="dropdown-menu">
                    <div>
                        <div>
                            <button
                                v-for="group in filters.groups"
                                v-on:click="setFiltersSelected({group: group})"
                                :class="'btn btn-filter btn-block '+ (group === filtersSelected.group ? 'active' : '') + ' ' + (groupClass(group))">
                                    {{ group }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dropdown dropdown-with-arrow">
                <label class="btn-select-label" data-toggle="dropdown" data-flip=false>
                    <input
                        :class="'btn btn-select ' + (filtersSelected.dateDay ? 'active' : '')"
                        type="button"
                        :value="filtersSelected.dateDay ? $options.filters.dateFormat(filtersSelected.dateDay) : 'DATE'"
                    >
                    <i class="icon icon-arrow"></i>
                </label>
                <div class="dropdown-menu with-datepicker">
                    <div>
                        <div v-if="minDate">
                            <vuejsDatepicker
                                @selected="setDateDaySelected"
                                :value="filtersSelected.dateDay !== null ? filtersSelected.dateDay.toDate() : ''"
                                :inline="true"
                                :disabledDates="disabledDates"
                                :open-date="minDate"
                                :full-month-name="true"
                                :language="datepickerLanguage"
                                :maximum-view="'day'"
                            ></vuejsDatepicker>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dropdown dropdown-with-arrow">
                <label class="btn-select-label" data-toggle="dropdown" data-flip=false>
                    <input
                        :class="'btn btn-select ' + (filtersSelected.city ? 'active' : '')"
                        type="button"
                        :value="filtersSelected.city ? filtersSelected.city : 'CITY'"
                    >
                    <i class="icon icon-arrow"></i>
                </label>
                <div class="dropdown-menu with-scroll">
                    <div class="js-show-more-wrapper js-custom-scrollbar show-more-wrapper sm" data-show-more-target="city">
                        <div>
                            <button
                                v-for="city in filters.cities"
                                v-on:click="setFiltersSelected({city: city})"
                                :class="'btn btn-filter btn-block '+ (city === filtersSelected.city ? 'active' : '')">
                                    {{ city }}
                            </button>
                        </div>
                    </div>
                    <button class="js-show-more show-more" data-show-more-target="city" data-show-more-text="Show less" data-scroll-window="true">
                        Show more
                    </button>
                </div>
            </div>
            <label class="btn-reset-label">
                <input
                    v-on:click="clearFiltersSelected"
                    class="btn btn-reset"
                    type="reset"
                    value="Clear All"
                >
            </label>
        </div>
    `
})