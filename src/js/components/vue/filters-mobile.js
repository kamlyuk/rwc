Vue.component('filters-mobile', {
    data: () => {
        return {
            filtersPreSelected: {}
        }
    },

    computed: {

        // To update filtersPreSelected when filtersSelected was changed in the store
        watchFiltersSelected () {
            this.filtersPreSelected = Object.assign({}, this.$store.state.filtersSelected);
        },

        timezoneOffsetMinutes () {
            return this.$store.state.timezoneOffsetMinutes;
        },

        filters () {
            return this.$store.getters.filters;
        }

    },

    methods: {

        setFiltersPreSelected(values) {
            for (let key in values) {
                
                if (
                    values[key] !== null &&
                    (
                        this.filtersPreSelected[key] === null ||
                        values[key].toString() !== this.filtersPreSelected[key].toString()
                    )
                ) {
                    this.filtersPreSelected[key] = values[key];
                }
                else {
                    this.filtersPreSelected[key] = null;
                }
            }

            return false;
        },

        setFiltersSelected() {
            this.$store.commit('replaceFiltersSelected', this.filtersPreSelected);
            return false;
        },

        clearFiltersSelected() {
            this.$store.commit('clearFiltersSelected');
            return false;
        },

        groupClass(group) {
            return this.$store.getters.groupClass(group);
        }
        
    },

    template: `
        <div class="js-form modal-content" :data-watch="watchFiltersSelected">
            <div class="modal-header">
                <h3 class="modal-title">Filters</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="icon icon-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="heading" data-toggle="collapse" data-target=".collapse-country">
                    Teams
                    <i class="icon icon-minus"></i>
                    <i class="icon icon-plus"></i>
                </h4>
                <div class="collapse show collapse-country">
                    <div class="btn-group-toggle-wrapper">
                        <button
                            v-for="team in filters.teams"
                            :class="'btn btn-filter btn-sm ' + (team === filtersPreSelected.team ? 'active' : '')"
                            v-on:click="setFiltersPreSelected({team: team})">
                            {{ team }}
                        </button>
                    </div>
                </div>
                <h4 class="heading collapsed" data-toggle="collapse" data-target=".collapse-pool">
                    Group
                    <i class="icon icon-minus"></i>
                    <i class="icon icon-plus"></i>
                </h4>
                <div class="collapse collapse-pool">
                    <div class="js-btn-group-toggle-wrapper btn-group-toggle-wrapper">
                        <button
                            v-for="group in filters.groups"
                            :class="'btn btn-filter btn-sm ' + (group === filtersPreSelected.group ? 'active' : '') + ' ' + groupClass(group)"
                            v-on:click="setFiltersPreSelected({group: group})">
                            {{ group }}
                        </button>
                    </div>
                </div>
                <h4 class="heading collapsed" data-toggle="collapse" data-target=".collapse-date">
                    Date
                    <i class="icon icon-minus"></i>
                    <i class="icon icon-plus"></i>
                </h4>
                <div class="collapse collapse-date">
                    <div class="js-btn-group-toggle-wrapper btn-group-toggle-wrapper">
                        <button
                        v-for="month in filters.months"
                        :class="'btn btn-filter btn-sm ' + (month === filtersPreSelected.dateMonth ? 'active' : '')"
                        v-on:click="setFiltersPreSelected({dateMonth: month, dateDay: null})">
                            {{ month }}
                        </button>
                    </div>
                </div>
                <h4 class="heading collapsed" data-toggle="collapse" data-target=".collapse-city">
                    City
                    <i class="icon icon-minus"></i>
                    <i class="icon icon-plus"></i>
                </h4>
                <div class="collapse collapse-city">
                    <div class="js-btn-group-toggle-wrapper btn-group-toggle-wrapper">
                        <button
                        v-for="city in filters.cities"
                        :class="'btn btn-filter btn-sm ' + (city === filtersPreSelected.city ? 'active' : '')"
                        v-on:click="setFiltersPreSelected({city: city})">
                            {{ city }}
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button v-on:click="clearFiltersSelected" class="reset btn btn-filter" data-dismiss="modal">Clear all</button>
                <button v-on:click="setFiltersSelected" class="apply btn btn-primary btn-gradient" data-dismiss="modal">Apply filter</button>
            </div>
        </div>
    `
})