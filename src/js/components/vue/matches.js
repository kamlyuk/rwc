Vue.component('matches', {

    data: () => {
        return {
            matchesFilteredMinLength: 4
        }
    },

    computed: {

        matches () {
            return this.$store.getters.matches;
        },

        filtersSelected () {
            return this.$store.state.filtersSelected;
        },

        isAnyFilterSelected() {

            for (key in this.filtersSelected) {
                if (this.filtersSelected[key] !== null) {
                    return true;
                }
            }

            return false
        },

        matchesFiltered() {

            let matchesFiltered = [];
            matchesFiltered = matchesFiltered.concat(this.matches);

            if (this.isAnyFilterSelected) {
                
                // All matches pass through every selected filter and remaining matches are returned.
                for (let key in this.filtersSelected) {
                    if (this.filtersSelected[key] !== null) {

                        matchesFiltered = matchesFiltered.filter((match) => {
                            
                            if (key === 'team') {
                                if (this.filtersSelected.team !== match.teams[0].name && this.filtersSelected.team !== match.teams[1].name) {
                                    return false;
                                }
                            }

                            else if (key === 'group') {
                                if (!match.group.faction || this.filtersSelected.group !== match.group.faction) {
                                    return false;
                                }
                            }

                            else if (key === 'dateDay') {
                                if (!match.dateThere || (this.filtersSelected.dateDay.format('YYYY MM DD') !== match.dateThere.format('YYYY MM DD'))) {
                                    return false;
                                }
                            }

                            else if (key === 'dateMonth') {
                                if (!match.dateThere || (this.filtersSelected.dateMonth !== match.dateThere.format('MMMM'))) {
                                    return false;
                                }
                            }

                            else if (key === 'city') {
                                if (!match.location.city || (this.filtersSelected.city !== match.location.city)) {
                                    return false;
                                }
                            }

                            else if (key === 'search') {

                                let searchText = this.filtersSelected.search.toLowerCase();

                                if (
                                    match.teams[0].name.toLowerCase().indexOf(searchText) !== 0 &&
                                    match.teams[1].name.toLowerCase().indexOf(searchText) !== 0 &&
                                    (!match.group.faction || (match.group.faction.toLowerCase().indexOf(searchText) !== 0)) &&
                                    (!match.dateThere || (match.dateThere.format('D MMMM').toLowerCase().indexOf(searchText) !== 0)) &&
                                    (!match.dateThere || (match.dateThere.format('MMMM').toLowerCase().indexOf(searchText) !== 0)) &&
                                    (!match.location.city || (match.location.city.toLowerCase().indexOf(searchText) !== 0))
                                ) {
                                    return false;
                                }
                            }

                            return true;
                        });
                    }
                }
            }

            return matchesFiltered;
        }

    },

    filters: {

        // For right vertical displaying
        spacesToNbsp(value) {
            return value ? value.replace(/\s/g, ' &nbsp; ') : '';
        }
        
    },

    template: `
        <div>
            <div class="js-show-more-wrapper" :class="matchesFiltered.length >= matchesFilteredMinLength ? 'show-more-wrapper lg' : ''" data-show-more-target="matches">
                <transition-group tag="div" class="js-matches matches">
                    <div class="card card-match" v-for="match in matchesFiltered" :key="match.id">
                        <div :class="'pool ' + match.group.class" v-html="$options.filters.spacesToNbsp(match.group.text)"></div>
                        <div class="match">
                            <div class="date">
                                {{ match.dateThereFormated || '&nbsp;' }}
                            </div>
                            <div class="sides">
                                <div class="side">
                                    <img class="flag-img" :src="match.teams[0].flagImg" :alt="match.teams[0].name">
                                    <div class="country">{{ match.teams[0].nameDisplay }}</div>
                                </div>
                                <div class="divider">
                                    v
                                </div>
                                <div class="side">
                                    <img class="flag-img" :src="match.teams[1].flagImg" :alt="match.teams[1].name">
                                    <div class="country">{{ match.teams[1].nameDisplay }}</div>
                                </div>
                            </div>
                            <div class="location">
                                {{ match.locationFull || '&nbsp;' }}
                            </div>
                        </div>
                    </div>
                    <p v-if="matchesFiltered.length === 0" :key="'no-matches'">No matches by selected filters.</p>
                </transition-group>
            </div>
            <div v-show="matchesFiltered.length >= matchesFilteredMinLength" class="d-lg-none text-center">
                <button
                    class="js-show-more btn btn-primary btn-gradient btn-lg"
                    data-show-more-target="matches"
                    data-show-more-text="Show Less"
                    data-scroll-window="true">
                        Show More
                </button>
            </div>
        </div>
    `
})