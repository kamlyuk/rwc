class Match {
    constructor(match) {
        const flagImgDefault =      'images/flags/sm/not-known.png';
        const groupClassDefault =   'pool-not-known';
        const isTeamDefault =       false;
        const teamNameDefault =     'not known';

        match.id =                  match.id                || Math.random().toString(36).substr(2);
        match.group =               match.group             || {};
        match.group.class =         match.group.class       || groupClassDefault;
        match.group.text =          match.group.text        || null;
        match.group.faction =       match.group.faction     || null;
        match.dateUTC =             match.dateUTC           || null;
        match.location =            match.location          || {};
        match.location.stadium =    match.location.stadium  || null;
        match.location.city =       match.location.city     || null;
        match.teams =               match.teams             || [];
        match.teams[0] =            match.teams[0]          || {};
        match.teams[0].isTeam =     match.teams[0].isTeam   || isTeamDefault;
        match.teams[0].name =       match.teams[0].name     || teamNameDefault;
        match.teams[0].flagImg =    match.teams[0].flagImg  || flagImgDefault;
        match.teams[1] =            match.teams[1]          || {};
        match.teams[1].isTeam =     match.teams[1].isTeam   || isTeamDefault;
        match.teams[1].name =       match.teams[1].name     || teamNameDefault;
        match.teams[1].flagImg =    match.teams[1].flagImg  || flagImgDefault;

        if (match.dateUTC && moment(match.dateUTC).isValid()){
            match.dateUTC = moment(match.dateUTC).utc();
        }
        else {
            match.dateUTC = null;
        }

        this.match = match;
    }

    get id() {
        return this.match.id;
    }

    get group() {
        return this.match.group;
    }

    get dateUTC() {
        return this.match.dateUTC;
    }
    
    get location() {
        return this.match.location;
    }
    
    get locationFull() {

        array = [];

        if (this.location.stadium)  array.push(this.location.stadium);
        if (this.location.city)     array.push(this.location.city);

        return array.join(', ');
    }

    get teams() {
        return this.match.teams;
    }
}