// Vue instance matches-filters

$(function () {
    $('.js-matches-filters').each(function() {

        // To don't close dropdown when user is switching month in datepicker
        $(document).on('click', '.vdp-datepicker__calendar header', function (e) {
            return false;
        });

        let matchesFilters = $(this);

        const store = new Vuex.Store({
            strict: true,
            state: {
                timezoneOffsetMinutes: 0,
                matches: [],
                filtersSelected: {
                    team: null,
                    group: null,
                    dateDay: null,
                    dateMonth: null,
                    city: null,
                    search: null
                }
            },
            getters: {

                matches(state) {

                    let matches = state.matches.slice();

                    state.matches.map((match) => {

                        // dateThere
                        if (match.dateUTC) {
                            match.dateThere = moment(match.dateUTC).utcOffset(state.timezoneOffsetMinutes);
                        }
                        else {
                            match.dateThere = null;
                        }

                        // dateThereFormated
                        if (match.dateThere) {
                            let hoursOffset = match.dateThere.utcOffset() / 60;
                            if (hoursOffset >= 0) {
                                hoursOffset = '+' + hoursOffset;
                            }
                            match.dateThereFormated = moment(match.dateThere).format('DD MMMM YYYY[, (GMT]' + hoursOffset + ') HH:mm');
                        }
                        else {
                            match.dateThereFormated = null;
                        }

                        // teams[].nameDisplay
                        match.teams.map((team) => {
                            team.nameDisplay = team.isTeam ? team.name.toUpperCase() : team.name;
                        });
                        
                    });

                    return matches;
                },

                filters(state, getters) {

                    let filters = {
                        teams: [],
                        groups: [],
                        dates: [],
                        months: [],
                        cities: [],
                        search: []
                    };

                    getters.matches.map((match) => {

                        // Teams
                        match.teams.map((team)=>{
                            if (team.isTeam && !filters.teams.includes(team.name)) {
                                filters.teams.push(team.name);
                                filters.search.push(team.name);
                            }
                        })

                        // Groups
                        if (match.group.faction && !filters.groups.includes(match.group.faction)) {
                            filters.groups.push(match.group.faction);
                            filters.search.push(match.group.faction);
                        }

                        // Dates
                        if (match.dateThere && !filters.dates.some((date) => {
                            return date.toString() === match.dateThere.toString()
                        })) {
                            filters.dates.push(match.dateThere);
                            filters.search.push(match.dateThere.format('D MMMM'));
                        }

                        // Months
                        if (match.dateThere && !filters.months.some((month) => {
                            return month === match.dateThere.format('MMMM');
                        })) {
                            filters.months.push(match.dateThere.format('MMMM'));
                            filters.search.push(match.dateThere.format('MMMM'));
                        }

                        // Cities
                        if (match.location.city && !filters.cities.includes(match.location.city)) {
                            filters.cities.push(match.location.city);
                            filters.search.push(match.location.city);
                        }

                    });

                    return filters;
                },

                // Make array of classes for groups
                groupsClasses(state, getters) {
                    let groupsClasses = [];

                    getters.matches.map((match) => {
                        if (match.group.faction && !groupsClasses.includes(match.group.class)) {
                            groupsClasses.push(match.group.class);
                        }
                    });

                    return groupsClasses;
                },

                // Get class name by group's name
                groupClass: (state, getters) => (group) => {
                    for (let i = 0; i < getters.filters.groups.length; i++) {
                        if (getters.filters.groups[i] === group) {
                            return getters.groupsClasses[i];
                        }
                    }
                }

            },
            mutations: {

                setTimezoneOffsetMinutes(state, minutes) {

                    // Check for right format before changing
                    if(!Number.isInteger(minutes))
                    {
                        console.error('Error in `store.mutations.setTimezoneOffsetMinutes()`. `timezoneOffsetMinutes` is not a number. `timezoneOffsetMinutes` were not changed.');
                    }
                    else {
                        state.timezoneOffsetMinutes = minutes;
                    }
                },

                addMatches(state, matches) {

                    // Check for right format before adding
                    const errorWhere = 'Error in `store.mutations.addMatches()`.'
                    const errorNotAdded = 'Match was not added.'

                    // If it is not an array
                    if (!Array.isArray(matches)) {
                        console.error(errorWhere + 'Matches are not an array. Matches were not added.');
                        return false;
                    }
                    
                    matches.map((match) => {
                        state.matches.push(new Match(match));
                    });

                },

                updateFiltersSelected(state, values) {

                    // Set value if it is not same or set null
                    for (key in values) {
                        if (
                            values[key] !== null &&
                            (
                                state.filtersSelected[key] === null ||
                                values[key].toString() !== state.filtersSelected[key].toString()
                            )
                        ) {
                            state.filtersSelected[key] = values[key];
                        }
                        else {
                            state.filtersSelected[key] = null;
                        }
                    }
                },

                replaceFiltersSelected(state, values) {
                    state.filtersSelected = Object.assign({}, values);
                },
                
                clearFiltersSelected(state) {
                    for (key in state.filtersSelected) {
                        state.filtersSelected[key] = null;
                    }
                }
            }
        })

        new Vue({
            el: '#app',
            store,
            mounted() {
                axios
                .get(matchesFilters.attr('data-matches'))
                .then((response) => {

                    const errorWhere = 'Error in `Vue.mounted()`.';

                    if (typeof response.data.matches === 'undefined') {
                        console.error(errorWhere, '`matches` are undefined or have incorrect JSON. Matches were not added.');
                    }
                    else {
                        this.$store.commit('addMatches', response.data.matches);
                    }

                    if (typeof response.data.timezoneOffsetMinutes === 'undefined') {
                        console.error(errorWhere, '`timezoneOffsetMinutes` is undefined or have incorrect JSON. `setTimezoneOffsetMinutes` were not changed.');
                    }
                    else {
                        this.$store.commit('setTimezoneOffsetMinutes', response.data.timezoneOffsetMinutes);
                    }
                });
            }
        });
    });
});