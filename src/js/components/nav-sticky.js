// Nav sticky component

$(function () {
    $('.js-nav-sticky').each(function () {

        var wrapperForCopy = $('.js-nav-sticky-wrapper').first();
        var originalNav = $(this);
        var originalNavWrapper = $(this).closest('.js-nav-tabs-wrapper');
        var cloneNavWrapper = originalNavWrapper.clone(true);
        var copyNav = cloneNavWrapper.find('.js-nav-sticky');
        
        cloneNavWrapper.addClass('copy hidden');
        
        wrapperForCopy.append(cloneNavWrapper);

        // Add sticky functionality for the copied nav
        $(window).off('scroll.nav-sticky').on('scroll.nav-sticky', function () {
            var wrapperForCopyHeight = wrapperForCopy.innerHeight();
            var copyNavHeight = copyNav.innerHeight();
            var offsetTop = wrapperForCopyHeight - copyNavHeight * 2;

            if ($(window).scrollTop() >= originalNav.offset().top + offsetTop) {
                cloneNavWrapper.removeClass('hidden');
            }

            else {
                cloneNavWrapper.addClass('hidden');
            }
        });
    });
});