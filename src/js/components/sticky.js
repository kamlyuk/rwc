// Sticky component

$(function () {
    $('.js-sticky').each(function(){
        
        var sticky = $(this);
        
        $(window).off('scroll.sticky').on('scroll.sticky', function(){
            
            var scrollTop = $(document).scrollTop();
            var stickyParent = sticky.parent();
            var stickyHeight = sticky.innerHeight();
            var stickyRealPosition = stickyParent.offset().top - $(window).height() + stickyHeight;
            
            if (scrollTop > 50)
                sticky.addClass('visible');
            else
                sticky.removeClass('visible');
            
            if (scrollTop < stickyRealPosition) {
                sticky.addClass('fixed');
                stickyParent.css('padding-top', stickyHeight + 'px');
            }
            else {
                sticky.removeClass('fixed');
                stickyParent.css('padding-top', '0');
            }
            
        });

        $(window).scroll();
    });
});