// Filters component

$(function () {
    $('.js-carousel').each(function () {

        var carousel = $(this);

        carousel.owlCarousel({
            nav: true,
            dots: false,
            items: 1
        });
    });
});