// Slider component

$(function () {
    $('.js-slider').each(function () {

        slider = $(this);
        var previews = $('.js-slider-preview');
        var modal = $('.modal-slider').first();
        var modalDialog = modal.find('.modal-dialog').first();
        var btnPrev = modalDialog.find('.js-slider-prev');
        var btnNext = modalDialog.find('.js-slider-next');
        var desktopThumbItem = 7;
        var slidesCountToCenterSlide = Math.ceil(desktopThumbItem / 2);

        slider.lightSlider({
            gallery: true,
            item: 1,
            thumbItem: desktopThumbItem,
            slideMargin: 0,
            thumbMargin: 20,
            controls: false,
            galleryMargin: 10,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        thumbItem: 3,
                        thumbMargin: 8,
                        galleryMargin: 28
                      }
                },
            ],
            onBeforeSlide: function() {

                // Show/hide white gradient on thumbnails
                var slideNum = slider.getCurrentSlideCount();
                var slidesTotal = slider.getTotalSlideCount();

                // Because getTotalSlideCount() returns incorrect value sometimes
                // It is fix for these cases
                if (typeof slideNum === 'string') {
                    slideNum = slideNum.slice(0, -1);
                    slideNum = parseInt(slideNum);
                    slideNum++;
                }

                if ((slidesCountToCenterSlide - slideNum) >= 0) {
                    modalDialog.removeClass('thumbnail-gradient-left');
                }
                else {
                    modalDialog.addClass('thumbnail-gradient-left');
                }

                if ((slidesTotal - slidesCountToCenterSlide - slideNum) < 0) {
                    modalDialog.removeClass('thumbnail-gradient-right');
                }
                else {
                    modalDialog.addClass('thumbnail-gradient-right');
                }
            }
        });
        
        // Open modal when image was clicked
        previews.off('click.slider').on('click.slider', function() {
            
            var indexNumber = $(this).attr('data-index-number');
            
            modal.modal('show');
            slider.goToSlide(indexNumber);
            
            return false;
        });
        
        // We need to refresh slider after opening the modal.
        // And use .active class for animation after refreshing.
        modal.off('show.bs.modal').on('show.bs.modal', function() {
            modalDialog.removeClass('active');
            $('body').addClass('backdrop-slider');
        });
        
        modal.off('shown.bs.modal').on('shown.bs.modal', function() {
            slider.refresh();
            modalDialog.addClass('active');
        });
        
        modal.off('hidden.bs.modal').on('hidden.bs.modal', function() {
            $('body').removeClass('backdrop-slider');
        });

        // Prev, next buttons
        btnPrev.off('click.slider').on('click.slider',  function() {
            slider.goToPrevSlide();
            return false;
        });
        
        btnNext.off('click.slider').on('click.slider',  function() {
            slider.goToNextSlide();
            return false;
        });
    });
});