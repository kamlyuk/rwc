// Nav tabs component

$(function () {
    var navTabs = $('.js-nav-tabs');

    if (navTabs.length > 0)
    {
        var navTabsOriginal = navTabs.closest('.nav-tabs-wrapper').filter(':not(.copy)');
        var navStickyWrapper = $('.js-nav-sticky-wrapper');
        var tabsLinks = $('.js-nav-link');
        var activeTabsName = '';
        var tabsWrapper = $('.js-tabs');
        var tabs = $('.js-tab');

        // Go to the Tab function
        var goTo = function(tabName) {

            // remember start wrapper's height for transition
            var currentHeight = tabsWrapper.innerHeight();
            tabsWrapper.css('height', currentHeight);
            tabsWrapper.addClass('active');

            // Close opened tab
            tabsLinks.removeClass('active');
            tabs.removeClass('active');

            // Find active tab
            var activeTabsLinks = tabsLinks.filter('[data-tab=' + tabName + ']');

            // If we not have tab with this name, switch first
            if (activeTabsLinks.length <= 0)
            {
                tabName = tabsLinks.first().attr('data-tab');
                activeTabsLinks = tabsLinks.filter('[data-tab=' + tabName + ']');
            }

            activeTabsLinks.addClass('active');
            
            var activeTab = tabs.filter('[data-tab=' + tabName + ']');
            activeTab.addClass('active');

            // For wrapper transition
            tabsWrapper.css('height', activeTab.innerHeight());

            // Make active tab visible by scrolling
            navTabs.each(function() {
                var tabActive = $(this).find('.js-nav-link.active').closest('.js-nav-item');
                $(this).animate({scrollLeft: tabActive.offset().left}, 500);
            });

            // Scroll to the Nav tab if user is below
            if ($(window).scrollTop() > navTabsOriginal.offset().top) {
                
                activeTab.one('transitionend.nav-tabs', function() {
                    $("html, body").animate({scrollTop: navTabsOriginal.offset().top - navStickyWrapper.height()}, 500);
                });

            }

            // Remove fixed height of wrapper after transition
            tabsWrapper.one('transitionend.nav-tabs', function() {
                tabsWrapper.css('height', '');
            });

            // Remember active tab's name
            activeTabsName = tabName;
        }

        // Turn on the active or first tab
        var hashInfo = window.location.hash.match(/(#tab-)(.+)$/i);

        if (hashInfo != null) {
            activeTabsName = hashInfo[2];
        }
        else {
            activeTabsName = tabsLinks.first().attr('data-tab');
            window.location.hash = '#tab-' + activeTabsName;
        }

        goTo(activeTabsName);

        // Click by the Tab link
        tabsLinks.off('click.nav-tabs').on('click.nav-tabs', function() {

            if ($(this).attr('data-tab') === activeTabsName)
                return false;

            goTo($(this).attr('data-tab'));
        });
    }
});