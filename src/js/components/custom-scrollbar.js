// Custom scrollbar

$(function () {
    
    // Init
    $('.js-custom-scrollbar').mCustomScrollbar({
        alwaysShowScrollbar: 1,
        scrollInertia: 300
    });

    // To don't close dropdown when user scrolls
    $(document).on('click', '.dropdown-menu .mCSB_draggerContainer', function (e) {
        return false;
    });
});