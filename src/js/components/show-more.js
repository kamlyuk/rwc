// Show more component

$(function () {
    $('.js-show-more').each(function () {

        var button = $(this);
        var target = button.attr('data-show-more-target');
        var blockWrapper = $('.js-show-more-wrapper[data-show-more-target="' + target + '"]').first();
        var blockChild = blockWrapper.children().first();
        var isShow = false;
        var buttonTextShowMore = button.text();
        var buttonTextShowLess = button.attr('data-show-more-text');
        var navStickyWrapper = $('.js-nav-sticky-wrapper');

        var params = {
            scrollWindow: 'false' // Scroll window to top of block Wrapper when 'Show less' was clicked
        };

        if (button.is("[data-scroll-window]"))
            params.scrollWindow = button.attr('data-scroll-window');

        button.off('click.show-more').on('click.show-more', function () {

            // Open
            if (isShow === false) {

                blockWrapper.css('height', blockChild.height());

                blockWrapper.one('transitionend.show-more', function () {
                    button.html(buttonTextShowLess);
                    blockWrapper.attr('data-height-auto', true);
                    blockWrapper.css('height', '');
                    isShow = true;
                });
            }

            // Close
            else {

                // Scroll window to top of block Wrapper when 'Show less' was clicked
                if (params.scrollWindow == 'true')
                    $("html, body").animate({ scrollTop: blockWrapper.offset().top - navStickyWrapper.height() * 2 }, 500, 'linear');

                blockWrapper.css('height', blockChild.height());
                blockWrapper.removeAttr('data-height-auto');
                blockWrapper.css('height', '');

                blockWrapper.one('transitionend.show-more', function () {
                    button.html(buttonTextShowMore);
                    isShow = false;
                });
            }

        return false;
        
        });
    });
});