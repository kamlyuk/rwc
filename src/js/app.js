/* Components */

// Vue components
//@@include('components/vue/datepicker-language.js')
//@@include('components/vue/match.js')
//@@include('components/vue/matches.js')
//@@include('components/vue/filters-search.js')
//@@include('components/vue/filters-desktop.js')
//@@include('components/vue/filters-mobile.js')

// Vue instances
//@@include('components/vue/instances/matches-filters.js')

// Other
//@@include('components/nav-sticky.js')
//@@include('components/nav-tabs.js')
//@@include('components/show-more.js')
//@@include('components/custom-scrollbar.js')
//@@include('components/carousel.js')
//@@include('components/sticky.js')
//@@include('components/slider.js')

// Turn off links with "#" href
$(function () {
    $('a[href="#"]').off('click.empty-link').on('click.empty-link', function() {
        return false;
    });
});