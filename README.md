## Gulp Starter 1.0 ##

#### 1. Setup Instructions:

   a. Clone
   ```
   git clone git@bitbucket.org:kamlyuk/rwc.git
   ```

   b. Install dependencies
   ```
   npm i
   ```

   c. Build project
   ```
   gulp