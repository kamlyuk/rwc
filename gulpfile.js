'use strict';

var gulp = require('gulp'),
    babel = require('gulp-babel'),
    browserSync = require('browser-sync'),
    cleanCSS = require('gulp-clean-css'),
    gutil = require('gulp-util'),
    image = require('gulp-image'),
    prefixer = require('gulp-autoprefixer'),
    fileinclude = require('gulp-file-include'),
    rimraf = require('rimraf'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    strip = require('gulp-strip-comments'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch'),
    merge = require('merge-stream'),
    concat = require('gulp-concat'),
    order = require('gulp-order');

function swallowError (error) {

  // If you want details of the error in the console
  console.log(error.toString());

  this.emit('end');
}

var folders = {
    src: 'src/',
    dst: './'
}

var path = {
    build: {
        html: folders.dst,
        css: folders.dst + 'css/',
        js: folders.dst + 'js/',
        images: folders.dst + 'images/',
        fonts: folders.dst + 'fonts/'
    },
    src: {
        html: folders.src + 'html/pages/*.html',
        css: folders.src + 'styles/app.scss',
        jsPlugins: folders.src + 'js/plugins.js',
        jsApp: folders.src + 'js/app.js',
        images: folders.dst + 'images/**/*.*',
        fonts:  [
            'node_modules/font-awesome-sass/assets/fonts/**/*.woff',
            'node_modules/font-awesome-sass/assets/fonts/**/*.woff2'
        ]
    },
    watch: {
        html: folders.src + 'html/**/*.html',
        css: folders.src + 'styles/**/*.scss',
        js: folders.src + 'js/**/*.js'
    },
    clean: ['css', 'js'],
    node: 'node_modules'
};

gulp.task('browser-sync', ['html:build', 'styles:build', 'js:build', ], function() {

    var files = [
        folders.dst + '*.*'
    ];

    browserSync.init(files, {
        server: {
            baseDir: folders.dst
        }
    });
});

gulp.task('html:build', function () {
    gulp.src(path.src.html) 
        .pipe(fileinclude({
            basepath: folders.src + 'html/',
            indent: true
        }))
        .on('error', swallowError)
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('styles:build', function () {
    gulp.src(path.src.css) 
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: [path.src.css],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }))
        .on('error', swallowError)
        .pipe(prefixer())
        .pipe(cleanCSS({level: {1: {specialComments: 0}}}))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('js:build', function () {

    var plugins = gulp.src(path.src.jsPlugins) 
        .pipe(fileinclude({
            prefix: '//@@',
            basepath: folders.src + 'js/'
        }))
        .on('error', swallowError);

    var app = gulp.src(path.src.jsApp) 
        .pipe(fileinclude({
            prefix: '//@@',
            basepath: folders.src + 'js/'
        }))
        .on('error', swallowError)
        .pipe(babel({
            presets: ['@babel/preset-env'],
            compact: false
        }))
        .on('error', swallowError);
        
    merge(plugins, app)
        .pipe(order(['plugins.js', 'app.js']))
        .on('error', swallowError)
        .pipe(sourcemaps.init())
        .on('error', swallowError)
        .pipe(concat('app.js'))
        .on('error', swallowError)
        .pipe(strip())
        .on('error', swallowError)
        .pipe(uglify())
        .on('error', swallowError)
        .pipe(sourcemaps.write('/'))
        .on('error', swallowError)
        .pipe(gulp.dest(path.build.js))
        .on('error', swallowError)
        .pipe(browserSync.reload({
            stream: true
        }))
        .on('error', swallowError);
});

gulp.task('images:build', function (cb) {
    gulp.src(path.src.images)
        .pipe(image({
            pngquant: true,
            optipng: false,
            zopflipng: true,
            jpegRecompress: false,
            mozjpeg: true,
            guetzli: false,
            gifsicle: true,
            svgo: true,
            concurrent: 10
        }))
        .pipe(gulp.dest(path.build.images));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
})

gulp.task('clean', function (cb) {
    for (var i = 0; i < path.clean.length; i++) {
        rimraf(path.clean[i], cb);
    }
});

gulp.task('build', [
    'html:build',
    'styles:build',
    'js:build',
    'fonts:build'
]);

gulp.task('default', ['build', 'browser-sync'], function () {
    gulp.watch(path.watch.html, ['html:build']);
    gulp.watch(path.watch.css, ['styles:build']);
    gulp.watch(path.watch.js, ['js:build']);
});
